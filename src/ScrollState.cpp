/*
    SPDX-FileCopyrightText: 2015 Lindsay Roberts <os@lindsayr.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "ScrollState.h"

#include <QWheelEvent>

#include <chrono>
#include <memory>
#include <cmath>

using namespace Konsole;
using time_point = std::chrono::time_point<std::chrono::system_clock>;
using seconds = std::chrono::duration<double, std::ratio<1, 1> >;

// Lines/second
constexpr double MIN_INERTIAL_SCROLL_SPEED = 16.0;
constexpr double INERTIAL_SCROLL_INITIAL_VELOCITY_MULTIPLIER = 1.5;
constexpr double INERTIAL_SCROLL_DECAY_FACTOR = 0.9; // Must be in [0, 1)


class ScrollState::Impl
{
public:
    void addWheelEvent(const QWheelEvent *wheel);
    void clearAll();
    int consumeLegacySteps(int stepSize);
    int consumeSteps(int pixelSize, int angleStepSize);

    void startInertialScrolling(double initialVelocity, QPoint sourcePosition);
    bool checkIsInertialScrolling();
    int consumeInertialScrollLines();
    void stopInertialScrolling();

    int _remainingScrollAngle;
    int _remainingScrollPixel;

    double _inertialScrollVelocity { 0.0 };
    double _unhandledInertialScrollLines { 0.0 };
    QPoint _inertialScrollingSourceEventPosition;
    bool _inertialScrolling { false };
    time_point _inertialScrollLastUpdateTime;
};

// Connect Impl to ScrollState
void ScrollState::addWheelEvent(const QWheelEvent *wheel) { pImpl->addWheelEvent(wheel); }
void ScrollState::clearAll() {  pImpl->clearAll(); }
int ScrollState::consumeLegacySteps(int stepSize) { return pImpl->consumeLegacySteps(stepSize); }
int ScrollState::consumeSteps(int pixelStepSize, int angleStepSize) { return pImpl->consumeSteps(pixelStepSize, angleStepSize); }

int ScrollState::angle() const { return pImpl->_remainingScrollAngle; }
int ScrollState::pixel() const { return pImpl->_remainingScrollPixel; }

void ScrollState::startInertialScrolling(double initialVelocity, QPoint sourceEventPoint) 
{ 
    pImpl->startInertialScrolling(initialVelocity, sourceEventPoint); 
}

void ScrollState::stopInertialScrolling() { pImpl->stopInertialScrolling(); }
bool ScrollState::isInertialScrolling() { return pImpl->checkIsInertialScrolling(); }
int ScrollState::consumeInertialScrollLines() { return pImpl->consumeInertialScrollLines(); }
QPoint ScrollState::getInertialScrollSourcePosition() const { return pImpl->_inertialScrollingSourceEventPosition; }
double ScrollState::getInertialScrollVelocity() const { return pImpl->_inertialScrollVelocity; }

ScrollState::ScrollState() : pImpl { new Impl() } { }
ScrollState::~ScrollState() { } // Needed for destruction of the std::unique_ptr

void ScrollState::Impl::addWheelEvent(const QWheelEvent *wheel)
{
    if ((wheel->angleDelta().y() != 0) && (wheel->pixelDelta().y() == 0)) {
        _remainingScrollPixel = 0;
    } else {
        _remainingScrollPixel += wheel->pixelDelta().y();
    }
    _remainingScrollAngle += wheel->angleDelta().y();
}

void ScrollState::Impl::clearAll()
{
    _remainingScrollPixel = 0;
    _remainingScrollAngle = 0;
}

int ScrollState::Impl::consumeLegacySteps(int stepsize)
{
    if (stepsize < 1) {
        stepsize = DEFAULT_ANGLE_SCROLL_LINE;
    }
    const int steps = _remainingScrollAngle / stepsize;
    _remainingScrollAngle -= steps * stepsize;
    if (steps != 0) {
        _remainingScrollPixel = 0;
    }
    return steps;
}

int ScrollState::Impl::consumeSteps(int pixelStepSize, int angleStepSize)
{
    if (_remainingScrollPixel != 0) {
        int steps = _remainingScrollPixel / pixelStepSize;
        _remainingScrollPixel -= steps * pixelStepSize;
        _remainingScrollAngle = 0;
        return steps;
    }
    int steps = _remainingScrollAngle / angleStepSize;
    _remainingScrollAngle -= steps * angleStepSize;
    _remainingScrollPixel = 0;
    return steps;
}

void ScrollState::Impl::startInertialScrolling(double initialVelocity, QPoint sourceEventPosition)
{
    if ((initialVelocity > 0) == (_inertialScrollVelocity > 0)) {
        _inertialScrollVelocity += initialVelocity * INERTIAL_SCROLL_INITIAL_VELOCITY_MULTIPLIER;
    }
    else {
        _inertialScrollVelocity = initialVelocity;
    }

    _inertialScrollingSourceEventPosition = sourceEventPosition;

    _inertialScrolling = true;
    _inertialScrollLastUpdateTime = std::chrono::system_clock::now();
}

void ScrollState::Impl::stopInertialScrolling()
{
    _inertialScrolling = false;
    _inertialScrollVelocity = 0.0;
    _unhandledInertialScrollLines = 0.0;
}

bool ScrollState::Impl::checkIsInertialScrolling()
{
    // Do we need to update _inertialScrolling?
    if (_inertialScrolling) {
        double scrollSpeed = std::abs(_inertialScrollVelocity);

        if (scrollSpeed < MIN_INERTIAL_SCROLL_SPEED) {
            stopInertialScrolling();
        }
    }

    return _inertialScrolling;
}

int ScrollState::Impl::consumeInertialScrollLines()
{
    if (!checkIsInertialScrolling()) {
        return 0;
    }

    double scrollSpeed = std::abs(_inertialScrollVelocity);
    time_point currentTime = std::chrono::system_clock::now();
    seconds deltaTime = currentTime - _inertialScrollLastUpdateTime;
    _inertialScrollLastUpdateTime = currentTime;

    double linesScrolled       = scrollSpeed * deltaTime.count() + _unhandledInertialScrollLines;
    int fullLinesScrolled      = (int) linesScrolled;

    // Push the remainder to the next attempt to consume lines.
    _unhandledInertialScrollLines = linesScrolled - fullLinesScrolled;

    // linesScrolled should be a signed quantity.
    if (_inertialScrollVelocity < 0.0) {
        fullLinesScrolled *= -1;
    }

    _inertialScrollVelocity *= std::pow(1.0 - INERTIAL_SCROLL_DECAY_FACTOR, deltaTime.count());
    return fullLinesScrolled;
}
