/*
    SPDX-FileCopyrightText: 2020-2020 Gustavo Carneiro <gcarneiroa@hotmail.com>
    SPDX-FileCopyrightText: 2007-2008 Robert Knight <robertknight@gmail.com>
    SPDX-FileCopyrightText: 1997, 1998 Lars Doelle <lars.doelle@on-line.de>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

// Own
#include "TouchInputTest.h"
#include "../terminalDisplay/input/TouchState.h"
#include "../terminalDisplay/TerminalFonts.h"
#include "../terminalDisplay/TerminalDisplay.h"

// Qt
#include <QDebug>
#include <QTouchEvent>

// Standard library
#include <thread>
#include <chrono>

// Testing
#include <qtest.h>

using namespace Konsole;
using TouchEvent = TouchState::TouchEvent;

constexpr TouchState::TerminalState stateAtEndWithScrollback {
    true, // Cursor visible
    true, // Scrolled to end
    true,  // We have scrollback
    false  // The command does not use mouse tracking.
};

constexpr TouchState::TerminalState stateAtEndWithoutScrollback {
    true, // Cursor visible
    true, // Scrolled to end
    false,  // We have no scrollback
    false   // The command does not use mouse tracking.
};

constexpr TouchState::TerminalState stateAtEndWithoutScrollbackWithMouseTracking {
    true, // Cursor visible
    true, // Scrolled to end
    false,  // We have no scrollback
    true   // The command does not use mouse tracking.
};

constexpr TouchState::TerminalState stateAboveEndWithScrollback {
    true, // Cursor visible
    false, // Scrolled to end
    true,  // We have scrollback
    false  // The command does not use mouse tracking.
};

constexpr TouchState::TerminalState stateAboveEndWithoutScrollback {
    false, // Cursor visible
    false, // Scrolled to end
    false,  // We have scrollback
    false  // The command does not use mouse tracking.
};

TouchInputTest::TouchInputTest() 
{
}

TouchInputTest::~TouchInputTest() { }

void TouchInputTest::testPositionReporting()
{
    TerminalDisplay display = TerminalDisplay(nullptr);
    TouchState touchState { display.terminalFont() };

    // One touch point.
    {
        touchState.registerTouchStart(TouchEvent { QPointF(1.0, 0.0) });
        QVERIFY(touchState.getLastPosition().toPoint().x() == 1);
        QVERIFY(touchState.getLastPosition().toPoint().y() == 0);
        QVERIFY(touchState.getStartPosition().toPoint().x() == 1);
        QVERIFY(touchState.getStartPosition().toPoint().y() == 0);

        touchState.registerTouchUpdate(TouchEvent { QPointF(3.0, 3.0) });
        QVERIFY(touchState.getLastPosition().toPoint().x() == 3);
        QVERIFY(touchState.getLastPosition().toPoint().y() == 3);
        QVERIFY(touchState.getStartPosition().toPoint().x() == 1);
        QVERIFY(touchState.getStartPosition().toPoint().y() == 0);

        touchState.registerTouchEnd(TouchEvent { QPointF(-1.0, 33.0) });
        QVERIFY(touchState.getLastPosition().toPoint().x() == -1);
        QVERIFY(touchState.getLastPosition().toPoint().y() == 33);
        QVERIFY(touchState.getStartPosition().toPoint().x() == 1);
        QVERIFY(touchState.getStartPosition().toPoint().y() == 0);
    }

    // Two touch points.
    {
        // It should average the positions of the points to determine
        // the gesture's position.
        touchState.registerTouchStart(TouchEvent { QPointF(1.0, 0.0), QPointF(-1.0, 2.0) });
        QVERIFY(touchState.getLastPosition().toPoint().x() == 0);
        QVERIFY(touchState.getLastPosition().toPoint().y() == 1);
        QVERIFY(touchState.getStartPosition().toPoint().x() == 0);
        QVERIFY(touchState.getStartPosition().toPoint().y() == 1);

        touchState.registerTouchUpdate(TouchEvent { QPointF(3.0, 3.0), QPointF(-3.0, -3.0) });
        QVERIFY(touchState.getLastPosition().toPoint().x() == 0);
        QVERIFY(touchState.getLastPosition().toPoint().y() == 0);
        QVERIFY(touchState.getStartPosition().toPoint().x() == 0);
        QVERIFY(touchState.getStartPosition().toPoint().y() == 1);

        // Switching to one point shouldn't change the start position
        touchState.registerTouchUpdate(TouchEvent { QPointF(3.0, 3.0) });
        QVERIFY(touchState.getLastPosition().toPoint().x() == 3);
        QVERIFY(touchState.getLastPosition().toPoint().y() == 3);
        QVERIFY(touchState.getStartPosition().toPoint().x() == 0);
        QVERIFY(touchState.getStartPosition().toPoint().y() == 1);

        // Switching to three touch points should reset the event -- changing
        // the start position.
        touchState.registerTouchUpdate
        (
            TouchEvent { QPointF(0.0, 0.0), QPointF(3.0, 3.0), QPointF(3.0, -3.0) }
        );
        QVERIFY(touchState.getLastPosition().toPoint().x() == 2);
        QVERIFY(touchState.getLastPosition().toPoint().y() == 0);
        QVERIFY(touchState.getStartPosition().toPoint().x() == 2);
        QVERIFY(touchState.getStartPosition().toPoint().y() == 0);

        touchState.registerTouchEnd(TouchEvent { QPointF(-1.0, 33.0) });
        QVERIFY(touchState.getLastPosition().toPoint().x() == -1);
        QVERIFY(touchState.getLastPosition().toPoint().y() == 33);
        QVERIFY(touchState.getStartPosition().toPoint().x() == 2);
        QVERIFY(touchState.getStartPosition().toPoint().y() == 0);
    }
}

void TouchInputTest::testSingleFingerScrollActionAboveEnd()
{
    using namespace std::chrono_literals;

    TerminalDisplay display = TerminalDisplay(nullptr);
    TerminalFont* font = display.terminalFont();
    TouchState touchState { display.terminalFont() };

    int lineHeight = font->fontHeight() + font->lineSpacing();
    int columnWidth  = font->fontWidth();

    TouchState::TerminalState terminalState {
        false, // Cursor not visible
        false, // Not scrolled to end
        true,  // We have scrollback
        false  // The command does not use mouse tracking.
    };

    {
        // (Very) fast vertical swipe should scroll
        touchState.registerTouchStart(TouchEvent { QPointF(1.0, 0.0) });

        // Initial action should be a request that we check back for feedback.
        QVERIFY(touchState.getNextAction(terminalState) == TouchState::CHECK_FOR_ACTION_AFTER_DELAY);

        touchState.registerTouchUpdate(TouchEvent { QPointF(1.0, lineHeight) });

        touchState.registerTouchUpdate(TouchEvent { QPointF(1.0, lineHeight * 2) });
        QVERIFY(touchState.getNextAction(terminalState) == TouchState::ACTION_SCROLL);
        QVERIFY(touchState.popAsVerticalScroll(terminalState) < 0);

        // Already scrolled, shouldn't have anything to do.
        QVERIFY(touchState.getNextAction(terminalState) == TouchState::ACTION_NULL);

        touchState.registerTouchUpdate(TouchEvent { QPointF(1.0, lineHeight * 2 + lineHeight / 2) });

        // Fractional change in lineheight, no scroll.
        QVERIFY(touchState.popAsVerticalScroll(terminalState) == 0);

        // Already scrolled, shouldn't have anything to do.
        QVERIFY(touchState.getNextAction(terminalState) == TouchState::ACTION_NULL);

        touchState.registerTouchUpdate(TouchEvent { QPointF(columnWidth * 2, lineHeight * 2) });
        std::this_thread::sleep_for(120ms);

        // Sideways: No scroll.
        QVERIFY(touchState.getNextAction(terminalState) == TouchState::ACTION_NULL);

        touchState.registerTouchEnd(TouchEvent { QPointF(0, -lineHeight * 18) });

        // Fling down: kinetic scroll down.
        QVERIFY(touchState.getNextAction(terminalState) == TouchState::ACTION_START_INERTIAL_SCROLL);
        QVERIFY(touchState.popAsVerticalScroll(terminalState) >= 1);
    }

    // Slower scroll: No kinetic scrolling.
    {
        touchState.registerTouchStart(TouchEvent { QPointF(1.0, 0.0) });

        touchState.registerTouchUpdate(TouchEvent { QPointF(1.0, lineHeight) });
        std::this_thread::sleep_for(200ms);

        touchState.registerTouchUpdate(TouchEvent { QPointF(1.0, lineHeight * 2) });
        std::this_thread::sleep_for(120ms);

        touchState.registerTouchEnd(TouchEvent { QPointF(1.0, lineHeight * 3) });
        QVERIFY(touchState.getNextAction(terminalState) == TouchState::ACTION_SCROLL);
    }
}

void TouchInputTest::testSingleFingerScrollActionNearEnd()
{
    using namespace std::chrono_literals;

    TerminalDisplay display = TerminalDisplay(nullptr);
    TerminalFont* font = display.terminalFont();
    TouchState touchState { display.terminalFont() };

    int lineHeight = font->fontHeight() + font->lineSpacing();
    int columnWidth  = font->fontWidth();

    // Fast single-finger swipe...
    {
        touchState.registerTouchStart(TouchEvent { QPointF(1.0, 0.0) });
        touchState.registerTouchUpdate(TouchEvent { QPointF(1.0, lineHeight) });

        // If we DO have scrollback, we should scroll,
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_SCROLL);

        // Otherwise, we shouldn't
        QVERIFY(touchState.getNextAction(stateAtEndWithoutScrollback) != TouchState::ACTION_SCROLL);

        touchState.registerTouchUpdate(TouchEvent { QPointF(1.0, lineHeight * 2) });
        // No scrolling when at the end.
        QVERIFY(touchState.getNextAction(stateAtEndWithoutScrollback) != TouchState::ACTION_SCROLL);

        // Above the end, still no scrollback. Trying to scroll is okay. So is ACTION_NULL.
        TouchState::ActionType actionAboveEndNoScrollback = touchState.getNextAction(stateAboveEndWithoutScrollback);
        QVERIFY(actionAboveEndNoScrollback == TouchState::ACTION_SCROLL || actionAboveEndNoScrollback == TouchState::ACTION_NULL);
    }

    // Slow single-finger swipe (near end) Should not scroll.
    {
        std::this_thread::sleep_for(400ms);

        touchState.registerTouchStart(TouchEvent { QPointF(0.0, 0.0) });

        std::this_thread::sleep_for(120ms);
        touchState.registerTouchUpdate(TouchEvent { QPointF(0.0, lineHeight / 2.0) });

        std::this_thread::sleep_for(120ms);
        touchState.registerTouchUpdate(TouchEvent { QPointF(0.0, lineHeight) });

        std::this_thread::sleep_for(120ms);
        touchState.registerTouchUpdate(TouchEvent { QPointF(0.0, lineHeight * 2) });

        // If at the end...
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_KEYPRESS);

        // If far from the end...
        QVERIFY(touchState.getNextAction(stateAboveEndWithScrollback) == TouchState::ACTION_SCROLL);
    }

    // Fast single-finger swipe? Should scroll.
    {
        touchState.registerTouchStart(TouchEvent { QPointF(0.0, 0.0) });

        std::this_thread::sleep_for(12ms);
        touchState.registerTouchUpdate(TouchEvent { QPointF(0.0, lineHeight / 2) });

        std::this_thread::sleep_for(12ms);
        touchState.registerTouchUpdate(TouchEvent { QPointF(0.0, lineHeight) });

        std::this_thread::sleep_for(12ms);
        touchState.registerTouchUpdate(TouchEvent { QPointF(0.0, lineHeight * 2) });

        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_SCROLL);
        QVERIFY(touchState.popAsVerticalScroll(stateAtEndWithScrollback) <= -1);
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_NULL);
    }

    // Move up, then swipe left/right, should still scroll up.
    {
        touchState.registerTouchStart(TouchEvent { QPointF(0.0, 0.0) });

        std::this_thread::sleep_for(12ms);
        touchState.registerTouchUpdate(TouchEvent { QPointF(0.0, lineHeight / 2) });

        std::this_thread::sleep_for(12ms);
        touchState.registerTouchUpdate(TouchEvent { QPointF(0.0, lineHeight) });

        std::this_thread::sleep_for(12ms);
        touchState.registerTouchUpdate(TouchEvent { QPointF(0.0, lineHeight * 2) });

        std::this_thread::sleep_for(25ms);
        touchState.registerTouchUpdate(TouchEvent { QPointF(columnWidth * 2, lineHeight * 2) });

        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_SCROLL);
        QVERIFY(touchState.popAsVerticalScroll(stateAtEndWithScrollback) <= -1);
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_NULL);

        std::this_thread::sleep_for(25ms);
        touchState.registerTouchUpdate(TouchEvent { QPointF(-columnWidth * 2, lineHeight * 2) });
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_NULL);
    }
}

void TouchInputTest::testDoubleFingerScroll()
{
    using namespace std::chrono_literals;

    TerminalDisplay display = TerminalDisplay(nullptr);
    TerminalFont* font = display.terminalFont();
    TouchState touchState { display.terminalFont() };

    int lineHeight = font->fontHeight() + font->lineSpacing();

    // Single finger => double finger.
    {
        touchState.registerTouchStart(TouchEvent { QPointF(1.0, 0.0) });
        std::this_thread::sleep_for(10ms);

        touchState.registerTouchUpdate(TouchEvent { QPointF(1.0, lineHeight) });
        std::this_thread::sleep_for(10ms);

        touchState.registerTouchUpdate(TouchEvent { QPointF(1.0, lineHeight * 2) });
        std::this_thread::sleep_for(10ms);

        // No scrolling when at the end.
        QVERIFY(touchState.getNextAction(stateAtEndWithoutScrollback) != TouchState::ACTION_SCROLL);

        {
            // ... even with mouse tracking
            TouchState::ActionType action = touchState.getNextAction(stateAtEndWithoutScrollbackWithMouseTracking);
            QVERIFY(action != TouchState::ACTION_SCROLL);
        }

        touchState.registerTouchUpdate(TouchEvent { QPointF(1.0, lineHeight), QPointF(3.0, lineHeight) });
        std::this_thread::sleep_for(10ms);

        {
            // Switching to two fingers should reset...
            TouchState::ActionType action = touchState.getNextAction(stateAtEndWithoutScrollbackWithMouseTracking);
            QVERIFY(action == TouchState::CHECK_FOR_ACTION_AFTER_DELAY);
            touchState.popAsActionDelay(stateAtEndWithScrollback);
        }

        touchState.registerTouchUpdate(TouchEvent { QPointF(1.0, -lineHeight), QPointF(3.0, -lineHeight) });
        std::this_thread::sleep_for(10ms);

        touchState.registerTouchUpdate(TouchEvent { QPointF(1.0, -2 * lineHeight), QPointF(3.0, -2 * lineHeight) });
        std::this_thread::sleep_for(10ms);

        // Two fingers, however, should scroll, but only with mouse tracking (if at end).
        QVERIFY(touchState.getNextAction(stateAtEndWithoutScrollbackWithMouseTracking) == TouchState::ACTION_SCROLL);

        // Above end, should scroll.
        QVERIFY(touchState.getNextAction(stateAboveEndWithScrollback) == TouchState::ACTION_SCROLL);
    }

    // Slow, two-finger action
    {
        touchState.registerTouchStart(TouchEvent { QPointF(1.0, 0.0) });
        std::this_thread::sleep_for(100ms);

        touchState.registerTouchUpdate(TouchEvent { QPointF(1.0, lineHeight), QPointF(1.0, -lineHeight) });
        std::this_thread::sleep_for(120ms);

        touchState.registerTouchUpdate(TouchEvent { QPointF(1.0, lineHeight * 2), QPointF(1.0, 0.0) });
        std::this_thread::sleep_for(120ms);

        touchState.registerTouchUpdate(TouchEvent { QPointF(1.0, lineHeight * 3), QPointF(1.0, lineHeight) });
        std::this_thread::sleep_for(120ms);

        touchState.registerTouchEnd(TouchEvent { QPointF(1.0, lineHeight * 4), QPointF(1.0, lineHeight * 2) });
        QVERIFY(touchState.getNextAction(stateAtEndWithoutScrollbackWithMouseTracking) == TouchState::ACTION_SCROLL);
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_SCROLL);
        QVERIFY(touchState.getNextAction(stateAboveEndWithScrollback) == TouchState::ACTION_SCROLL);
        QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_SCROLL);
    }
}

void TouchInputTest::testSelectionAction()
{
    using namespace std::chrono_literals;

    TerminalDisplay display = TerminalDisplay(nullptr);
    TerminalFont* font = display.terminalFont();
    TouchState touchState { display.terminalFont() };

    int lineHeight = font->fontHeight() + font->lineSpacing();
    int columnWidth  = font->fontWidth();

    // Slow, right swipe
    {
        touchState.registerTouchStart(TouchEvent { QPointF(0.0, 0.0) });
        std::this_thread::sleep_for(100ms);

        touchState.registerTouchUpdate(TouchEvent { QPointF(columnWidth, 0.0) });
        std::this_thread::sleep_for(120ms);

        touchState.registerTouchUpdate(TouchEvent { QPointF(columnWidth * 2, 0.0) });
        std::this_thread::sleep_for(120ms);

        touchState.registerTouchUpdate(TouchEvent { QPointF(columnWidth * 3, 0.0) });
        std::this_thread::sleep_for(120ms);

        QVERIFY(touchState.getNextAction(stateAtEndWithoutScrollbackWithMouseTracking) == TouchState::ACTION_KEYPRESS);
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_KEYPRESS);
        QVERIFY(touchState.getNextAction(stateAboveEndWithScrollback) == TouchState::ACTION_MOUSEBUTTON_DOWN);
        QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_MOUSEBUTTON_DOWN);

        QVERIFY(touchState.popAsMouseButton(stateAboveEndWithoutScrollback) == Qt::MouseButton::LeftButton);
        QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_NULL);

        touchState.registerTouchUpdate(TouchEvent { QPointF(columnWidth * 40.1, 0.0) });
        std::this_thread::sleep_for(120ms);

        // We .popAsMouseButton-ed! As such, the next actions should also be interpreted as mouse button events!s
        QVERIFY(touchState.getNextAction(stateAtEndWithoutScrollbackWithMouseTracking) == TouchState::ACTION_MOUSEBUTTON_MOVE);
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_MOUSEBUTTON_MOVE);

        QVERIFY(touchState.getNextAction(stateAboveEndWithScrollback) == TouchState::ACTION_MOUSEBUTTON_MOVE);
        QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_MOUSEBUTTON_MOVE);

        QVERIFY(touchState.popAsMouseButton(stateAboveEndWithoutScrollback) == Qt::MouseButton::LeftButton);
        QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_NULL);

        touchState.registerTouchEnd(TouchEvent { QPointF(columnWidth * 4, 0.0) });
        std::this_thread::sleep_for(120ms);

        QVERIFY(touchState.getNextAction(stateAtEndWithoutScrollbackWithMouseTracking) == TouchState::ACTION_MOUSEBUTTON_UP);
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_MOUSEBUTTON_UP);
        QVERIFY(touchState.getNextAction(stateAboveEndWithScrollback) == TouchState::ACTION_MOUSEBUTTON_UP);
        QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_MOUSEBUTTON_UP);

        QVERIFY(touchState.popAsMouseButton(stateAboveEndWithoutScrollback) == Qt::MouseButton::LeftButton);
        QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_NULL);
    }

    // Fast left swipe
    {
        touchState.registerTouchStart(TouchEvent { QPointF(0.0, 0.0) });
        std::this_thread::sleep_for(10ms);

        touchState.registerTouchUpdate(TouchEvent { QPointF(columnWidth, 0.0) });
        std::this_thread::sleep_for(12ms);

        touchState.registerTouchUpdate(TouchEvent { QPointF(columnWidth * 2, 0.0) });
        std::this_thread::sleep_for(12ms);

        QVERIFY(touchState.getNextAction(stateAtEndWithoutScrollbackWithMouseTracking) == TouchState::ACTION_MOUSEBUTTON_DOWN);
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_MOUSEBUTTON_DOWN);
        QVERIFY(touchState.getNextAction(stateAboveEndWithScrollback) == TouchState::ACTION_MOUSEBUTTON_DOWN);
        QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_MOUSEBUTTON_DOWN);

        QVERIFY(touchState.popAsMouseButton(stateAboveEndWithoutScrollback) == Qt::MouseButton::LeftButton);
        QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_NULL);

        touchState.registerTouchUpdate(TouchEvent { QPointF(columnWidth * 4, lineHeight * 4) });
        std::this_thread::sleep_for(120ms); // Slowing down should continue to select.
        QVERIFY(touchState.getNextAction(stateAtEndWithoutScrollbackWithMouseTracking) == TouchState::ACTION_MOUSEBUTTON_MOVE);
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_MOUSEBUTTON_MOVE);
        QVERIFY(touchState.getNextAction(stateAboveEndWithScrollback) == TouchState::ACTION_MOUSEBUTTON_MOVE);
        QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_MOUSEBUTTON_MOVE);

        QVERIFY(touchState.popAsMouseButton(stateAboveEndWithoutScrollback) == Qt::MouseButton::LeftButton);
        QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_NULL);

        touchState.registerTouchEnd(TouchEvent { QPointF(columnWidth * 4, 0.0) });
        std::this_thread::sleep_for(200ms);

        QVERIFY(touchState.getNextAction(stateAtEndWithoutScrollbackWithMouseTracking) == TouchState::ACTION_MOUSEBUTTON_UP);
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_MOUSEBUTTON_UP);
        QVERIFY(touchState.getNextAction(stateAboveEndWithScrollback) == TouchState::ACTION_MOUSEBUTTON_UP);
        QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_MOUSEBUTTON_UP);

        QVERIFY(touchState.popAsMouseButton(stateAboveEndWithoutScrollback) == Qt::MouseButton::LeftButton);
        QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_NULL);
    }
}

void TouchInputTest::testKeypressAction()
{
    using namespace std::chrono_literals;

    TerminalDisplay display = TerminalDisplay(nullptr);
    TerminalFont* font = display.terminalFont();
    TouchState touchState { display.terminalFont() };

    int lineHeight = font->fontHeight() + font->lineSpacing();
    int columnWidth  = font->fontWidth();

    // Slow, right swipe
    {
        touchState.registerTouchStart(TouchEvent { QPointF(0.0, 0.0) });
        std::this_thread::sleep_for(100ms);

        touchState.registerTouchUpdate(TouchEvent { QPointF(columnWidth, 0.0) });
        std::this_thread::sleep_for(120ms);

        touchState.registerTouchUpdate(TouchEvent { QPointF(columnWidth * 2, 0.0) });
        std::this_thread::sleep_for(12ms);

        touchState.registerTouchUpdate(TouchEvent { QPointF(columnWidth * 3, 0.0) });
        std::this_thread::sleep_for(12ms);

        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_KEYPRESS);

        QVERIFY(touchState.popAsKeyEquivalent(stateAtEndWithScrollback) == Qt::Key::Key_Right);
        QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_NULL);

        touchState.registerTouchUpdate(TouchEvent { QPointF(columnWidth * 3, lineHeight * 4) });
        std::this_thread::sleep_for(12ms);

        // Swiping up shouldn't produce up/down keypress events, we started with a side-to-side swipe!
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_NULL);

        touchState.registerTouchUpdate(TouchEvent { QPointF(columnWidth * 6, 0.0) });
        std::this_thread::sleep_for(120ms);

        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_KEYPRESS);

        // Moving more than two columns => at least two keys to handle.
        QVERIFY(touchState.popAsKeyEquivalent(stateAtEndWithScrollback) == Qt::Key::Key_Right);
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_KEYPRESS);

        touchState.registerTouchEnd(TouchEvent { QPointF(columnWidth * -4, 0.0) });
        std::this_thread::sleep_for(120ms);
        
        // Should still have unhandled events... but to the left.
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_KEYPRESS);
        QVERIFY(touchState.popAsKeyEquivalent(stateAtEndWithScrollback) == Qt::Key::Key_Left);
    }

    // Slow, upwards swipe
    {
        touchState.registerTouchStart(TouchEvent { QPointF(0.0, 0.0) });
        std::this_thread::sleep_for(200ms);

        touchState.registerTouchUpdate(TouchEvent { QPointF(0.0, lineHeight * 2) });
        std::this_thread::sleep_for(200ms);

        touchState.registerTouchUpdate(TouchEvent { QPointF(0.0, lineHeight * 3) });
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_KEYPRESS);
        QVERIFY(touchState.popAsKeyEquivalent(stateAtEndWithScrollback) == Qt::Key::Key_Down);
        QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_NULL);
    }

    // Two-finger swipe, left.
    {
        touchState.registerTouchStart
        (
            TouchEvent { QPointF(0.0, 0.0) }
        );
        std::this_thread::sleep_for(200ms);

        touchState.registerTouchUpdate
        (
            TouchEvent { QPointF(0.0, lineHeight * 2) }
        );
        std::this_thread::sleep_for(200ms);

        touchState.registerTouchUpdate
        (
            TouchEvent { QPointF(0.0, lineHeight * 3) }
        );

        // Can start as another action...
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_KEYPRESS);

        touchState.registerTouchUpdate
        (
            TouchEvent { QPointF(0.0, lineHeight * 3), QPointF(columnWidth, lineHeight * 2) }
        );
        std::this_thread::sleep_for(200ms);

        // ... but resets when switches to two touch points.
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::CHECK_FOR_ACTION_AFTER_DELAY);
        touchState.popAsActionDelay(stateAtEndWithScrollback);

        touchState.registerTouchUpdate
        (
            TouchEvent { QPointF(-columnWidth * 2, lineHeight * 3), QPointF(-columnWidth, lineHeight * 2) }
        );
        std::this_thread::sleep_for(200ms);

        touchState.registerTouchEnd
        (
            TouchEvent { QPointF(-columnWidth * 8, lineHeight * 3), QPointF(-columnWidth * 7, lineHeight * 2) }
        );

        QVERIFY(touchState.getNextAction(stateAtEndWithoutScrollback) == TouchState::ACTION_KEYPRESS);
        QVERIFY(touchState.popAsKeyEquivalent(stateAtEndWithScrollback) == Qt::Key::Key_Escape);
        QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_NULL);
    }
}

void TouchInputTest::testClickAction()
{
    using namespace std::chrono_literals;

    TerminalDisplay display = TerminalDisplay(nullptr);
    TouchState touchState { display.terminalFont() };

    touchState.registerTouchStart(TouchEvent { QPointF(0.0, 0.0) });
    std::this_thread::sleep_for(200ms);

    touchState.registerTouchUpdate(TouchEvent { QPointF(0.0, 0.0) });
    std::this_thread::sleep_for(600ms);

    touchState.registerTouchUpdate(TouchEvent { QPointF(0.0, 0.0) });
    QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_SHOW_FEEDBACK);
    QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_SHOW_FEEDBACK);
    QVERIFY(touchState.getNextAction(stateAtEndWithoutScrollbackWithMouseTracking) == TouchState::ACTION_SHOW_FEEDBACK);
    QVERIFY(touchState.popAsTouchFeedback(stateAtEndWithoutScrollback) == TouchState::LONG_TAP_FEEDBACK);

    touchState.registerTouchEnd(TouchEvent { QPointF(0.0, 0.0) });
    QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_CLICK);
    QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_CLICK);
    QVERIFY(touchState.getNextAction(stateAtEndWithoutScrollbackWithMouseTracking) == TouchState::ACTION_CLICK);
    QVERIFY(touchState.popAsMouseButton(stateAboveEndWithoutScrollback) == Qt::MouseButton::RightButton);

    // Soon, but not too soon, after the start of the previous, we should get a left click.
    touchState.registerTouchStart(TouchEvent { QPointF(0.0, 0.0) });
    std::this_thread::sleep_for(20ms);

    touchState.registerTouchEnd(TouchEvent { QPointF(0.0, 0.0) });
    std::this_thread::sleep_for(20ms);

    QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_CLICK);
    QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_CLICK);
    QVERIFY(touchState.getNextAction(stateAtEndWithoutScrollbackWithMouseTracking) == TouchState::ACTION_CLICK);
    QVERIFY(touchState.popAsMouseButton(stateAboveEndWithoutScrollback) == Qt::MouseButton::LeftButton);

    // Then, a left double-click.
    touchState.registerTouchStart(TouchEvent { QPointF(0.0, 0.0) });
    std::this_thread::sleep_for(20ms);

    touchState.registerTouchEnd(TouchEvent { QPointF(0.0, 0.0) });
    std::this_thread::sleep_for(20ms);

    QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_DBCLICK);
    QVERIFY(touchState.popAsMouseButton(stateAboveEndWithoutScrollback) == Qt::MouseButton::LeftButton);

    // Then, a left tripple-click.
    touchState.registerTouchStart(TouchEvent { QPointF(0.0, 0.0) });
    std::this_thread::sleep_for(20ms);

    touchState.registerTouchEnd(TouchEvent { QPointF(0.0, 0.0) });
    std::this_thread::sleep_for(20ms);

    QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_TRIPPLE_CLICK);
    QVERIFY(touchState.popAsMouseButton(stateAboveEndWithoutScrollback) == Qt::MouseButton::LeftButton);
}

void TouchInputTest::testDragAction()
{
    using namespace std::chrono_literals;

    TerminalDisplay display = TerminalDisplay(nullptr);
    TerminalFont* font = display.terminalFont();
    TouchState touchState { font };

    int lineHeight = font->fontHeight() + font->lineSpacing();
    int columnWidth  = font->fontWidth();

    touchState.registerTouchStart(TouchEvent { QPointF(0.0, 0.0) });
    std::this_thread::sleep_for(200ms);

    touchState.registerTouchUpdate(TouchEvent { QPointF(0.0, 0.0) });
    std::this_thread::sleep_for(600ms);

    touchState.registerTouchUpdate(TouchEvent { QPointF(0.0, 0.0) });
    std::this_thread::sleep_for(100ms);

    QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_SHOW_FEEDBACK);
    QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_SHOW_FEEDBACK);
    QVERIFY(touchState.getNextAction(stateAtEndWithoutScrollbackWithMouseTracking) == TouchState::ACTION_SHOW_FEEDBACK);
    QVERIFY(touchState.popAsTouchFeedback(stateAtEndWithoutScrollback) == TouchState::LONG_TAP_FEEDBACK);

    touchState.registerTouchUpdate(TouchEvent { QPointF(columnWidth * 8, 0.0) });
    QVERIFY(touchState.getNextAction(stateAtEndWithScrollback) == TouchState::ACTION_DRAG);
    QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_DRAG);
    QVERIFY(touchState.getNextAction(stateAtEndWithoutScrollbackWithMouseTracking) == TouchState::ACTION_DRAG);
    QVERIFY(touchState.popAsMouseButton(stateAboveEndWithoutScrollback) == Qt::MouseButton::LeftButton);

    // Should only be a drag action once -- Qt handles drag actions after initial ACTION_DRAG
    touchState.registerTouchUpdate(TouchEvent { QPointF(columnWidth * 8, lineHeight * 4) });

    QVERIFY(touchState.getNextAction(stateAboveEndWithoutScrollback) == TouchState::ACTION_NULL);
}

QTEST_MAIN(TouchInputTest)
