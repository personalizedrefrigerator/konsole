/*
    SPDX-FileCopyrightText: 2020-2020 Gustavo Carneiro <gcarneiroa@hotmail.com>
    SPDX-FileCopyrightText: 2007-2008 Robert Knight <robertknight@gmail.com>
    SPDX-FileCopyrightText: 1997, 1998 Lars Doelle <lars.doelle@on-line.de>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#pragma once

#include <QObject>

namespace Konsole
{
// Forward declarations:
class TerminalFont;
class TerminalDisplay;

class TouchInputTest : public QObject
{
    Q_OBJECT
public:
    TouchInputTest();
    ~TouchInputTest();

private Q_SLOTS:
    void testPositionReporting();
    void testSingleFingerScrollActionAboveEnd();
    void testSingleFingerScrollActionNearEnd();
    void testDoubleFingerScroll();
    void testSelectionAction();
    void testKeypressAction();
    void testClickAction();
    void testDragAction();
};

}