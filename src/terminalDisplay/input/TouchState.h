/*
    SPDX-FileCopyrightText: 2020-2020 Gustavo Carneiro <gcarneiroa@hotmail.com>
    SPDX-FileCopyrightText: 2007-2008 Robert Knight <robertknight@gmail.com>
    SPDX-FileCopyrightText: 1997, 1998 Lars Doelle <lars.doelle@on-line.de>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#pragma once

#include <memory>

#include <qnamespace.h> // Qt::Key
#include <QPoint>
#include <QPointF>
#include <QList>
#include <initializer_list>

#include "konsoleprivate_export.h"

class QTouchEvent;

namespace Konsole {
class TerminalFont;


/**
 * @brief Represents a sequence of touch events.
 * 
 * TouchState maps a sequence of touch events to their corresponding actions.
 */
class KONSOLEPRIVATE_EXPORT TouchState 
{
public:
    enum ActionType { 
        ACTION_NULL,
        ACTION_KEYPRESS,
        ACTION_MOUSEBUTTON_DOWN, 
        ACTION_MOUSEBUTTON_MOVE,
        ACTION_MOUSEBUTTON_UP,
        ACTION_SHOW_FEEDBACK,
        CHECK_FOR_ACTION_AFTER_DELAY,
        ACTION_CLICK,
        ACTION_DBCLICK,
        ACTION_TRIPPLE_CLICK,
        ACTION_SCROLL,
        ACTION_START_INERTIAL_SCROLL,
        ACTION_DRAG
    };

    enum FeedbackType {
        LONG_TAP_FEEDBACK,
        NO_FEEDBACK
    };

public:
    /**
     * @brief Represents the current state of the terminal.
     */
    struct TerminalState {
        bool cursorIsVisible;
        bool scrolledToEnd;
        bool haveScrollback;
        bool commandUsesMouseTracking;
    };

    /**
     * @brief Represents a single touch event.
     * 
     * Used for synthesized touch events -- Qt
     * QTouchEvent::TouchPoint may not be constructable with
     * custom positions. 
     * 
     */
    struct TouchEvent {
        /**
         * @brief Construct using a list of points.
         * @param points are copied into this.
         */
        TouchEvent(const QList<QPointF>& points);
        TouchEvent(std::initializer_list<QPointF>);

        /**
         * @brief Convert from a QTouchEvent to a TouchEvent.
         */
        TouchEvent(const QTouchEvent*);

        QList<QPointF> touchPoints;
    };

public:
    TouchState(TerminalFont* font);
    ~TouchState();

    bool registerTouchStart (const TouchEvent& event);
    void registerTouchUpdate(const TouchEvent& event);
    void registerTouchEnd   (const TouchEvent& event);
    void update();

    /**
     * @brief Get the Qt::Key corresponding to the last unhandled portion of
     * the gesture.
     * 
     * @requires getNextAction to return ActionType::ACTION_KEYPRESS.
     * 
     * @param state describes the current state of the terminal.
     * @returns Qt::Key::Key_unknown if the sequence corresponds to no key sequence.
     */
    Qt::Key popAsKeyEquivalent(const TerminalState& state);

    /**
     * @brief Take all unhandled lines of vertical scroll. 
     * 
     * @requires getNextAction to return ActionType::ACTION_SCROLL.
     * 
     * @param state describes the current state of the terminal.
     * @returns the number of lines to scroll, positive if up,
     * negative if down.
     */
    int popAsVerticalScroll(const TerminalState& state);

    /**
     * @brief Take the type of feedback to show for the remaining part
     *  of the gesture.
     * 
     * @requires getNextAction to return ActionType::ACTION_SHOW_FEEDBACK.
     * 
     * @param state describes the current state of the terminal.
     * @returns the type of feedback to display.
     */
    FeedbackType popAsTouchFeedback(const TerminalState& state);

    /**
     * @brief Interpret unhandled input as time-based action, to be
     *   checked for after a number of seconds.
     * 
     * Although not required, if getNextAction returns 
     * ActionType::CHECK_FOR_ACTION_AFTER_DELAY, this should be called.
     * 
     * @returns the number of seconds after which the action may
     *   occur.
     */
    double popAsActionDelay(const TerminalState& state);

    /**
     * @brief Interpret unhandled portions of the gesture as a click/mouse move.
     * 
     * @requires getNextAction to return ACTION_CLICK, 
     *  ACTION_MOUSEBUTTON_DOWN, ACTION_MOUSEBUTTON_MOVE,
     *  ACTION_MOUSEBUTTON_UP, ACTION_DBCLICK, or ACTION_TRIPPLE_CLICK.
     * 
     * @param state describes the current state of the terminal.
     * @returns the equivalent mosuse button used for the gesture.
     */
    Qt::MouseButton popAsMouseButton(const TerminalState& state);

    /**
     * @brief Based on the current state of the terminal and the input
     * sequence, determe how this state should be interpreted.
     * 
     * @param state describes the current state of the terminal.
     * @return The type of action that should be taken.
     */
    ActionType getNextAction(const TerminalState& state) const;

    /**
     * @returns the last (x, y) position of the gesture in pixels.
     */
    QPointF getLastPosition() const;

    /**
     * @returns the starting (x, y) position of the gesture.
     */
    QPointF getStartPosition() const;

    /**
     * @returns The average velocity in (cols, lines) of the
     *  center of the gesture.
     * 
     * Note that this is in rows and columns.
     */
    QPointF getAverageVelocityInChars() const;

private:
    class Impl; std::unique_ptr<Impl> pImpl;
};

}
