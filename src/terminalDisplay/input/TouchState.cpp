/*
    SPDX-FileCopyrightText: 2020-2020 Gustavo Carneiro <gcarneiroa@hotmail.com>
    SPDX-FileCopyrightText: 2007-2008 Robert Knight <robertknight@gmail.com>
    SPDX-FileCopyrightText: 1997, 1998 Lars Doelle <lars.doelle@on-line.de>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "TouchState.h"
#include "../../terminalDisplay/TerminalFonts.h"

#include <chrono>
#include <cmath>

#include <QTouchEvent>
#include <QPointF>

#include <QDebug>

using namespace Konsole;
using TouchPoint = QTouchEvent::TouchPoint;
using time_point = std::chrono::time_point<std::chrono::system_clock>;

// Duration in seconds, seconds (as a number) are represented by a double.
using duration_seconds = std::chrono::duration<double, std::ratio<1, 1> >;
using seconds = double;

// Time to determine whether the gesture is a horizontal,
//vertical, tap, etc.
constexpr seconds GESTURE_TAP_MIN_LENGTH = 0.0;
constexpr seconds GESTURE_LONG_TAP_MIN_LENGTH = 0.5;

// Scroll gesture interpretation will be prioritized 
//over keypress gesture if the gesture (thus far) is shorter
//than GESTURE_KEYPRESS_MIN_DURATION.
constexpr seconds GESTURE_KEYPRESS_MIN_DURATION = 0.1;

// Prioritize other gestures over keypress gestures
// if faster than this (in cols/second and rows/second, respectively).
constexpr double GESTURE_KEYPRESS_MAX_COL_SPEED = 28.0;
constexpr double GESTURE_KEYPRESS_MAX_LINE_SPEED = 12.0;

// Inertial scrolling starts if scrolling & when ended,
// the speed is greater than this.
constexpr double GESTURE_FLING_MIN_LINE_SPEED = 43.0;

// Open a context menu after a tap with a duration equal to
//this number of seconds.
constexpr seconds STATIONARY_MIN_TIME_FOR_CONTEXT_MENU = 0.6;
constexpr seconds MAX_SECONDS_MOUSELIKE_EVENT = 0.4;
constexpr seconds PEEK_ACTION_EXTRA_DELAY = 0.1;

// Distance traveled by the center of a gesture must be greater than this for
// it to be in a particular direction.
constexpr double GESTURE_DETERMINATION_LINES = 1.0;
constexpr double GESTURE_DETERMINATION_COLS  = 2.0;

// If a gesture that starts with a long tap, allow greater distance
// before considering the gesture a long-tap-drag gesture.
constexpr double GESTURE_PRESS_TO_DRAG_DETERMINATION_LINES = 3 * GESTURE_DETERMINATION_LINES;
constexpr double GESTURE_PRESS_TO_DRAG_DETERMINATION_COLS = 3 * GESTURE_DETERMINATION_COLS;

class TouchState::Impl
{
public:
    Impl(TerminalFont* font);

public:
    bool registerTouchStart (const TouchEvent& event);
    void registerTouchUpdate(const TouchEvent& event);
    void registerTouchEnd   (const TouchEvent& event);
    void update();
    
    Qt::Key popAsKeyEquivalent();
    int popAsVerticalScroll(const TerminalState& state);
    FeedbackType popAsTouchFeedback(const TerminalState& state);
    Qt::MouseButton popAsMouseButton(const TerminalState& state);
    seconds popAsActionDelay(const TerminalState& state);
    ActionType getNextAction(const TerminalState& state) const;

    double getAverageYVelocity() const;
    double getAverageXVelocity() const;

private:
    void reset(const TouchEvent& event);
    
    Qt::Key peekKeyEquivalent() const;
    int peekVerticalScroll() const;
    FeedbackType peekTouchFeedback() const;
    Qt::MouseButton peekMouseButton() const;
    seconds peekActionDelay() const;
    
public:
    // The general shape of the gesture.
    enum ActionDirection 
    { 
        HORIZONTAL_ACTION,
        VERTICAL_ACTION,
        STATIONARY_ACTION,
        UNDETERMINED_DIRECTION
    };

    TerminalFont* m_font;

    QPointF m_lastCenter, m_startCenter;
    time_point m_sequenceStartTime;
    seconds m_gestureDuration; // Length of the gesture so far (in time)

    Qt::Key m_lastKeyResult;
    ActionType m_lastAction;

    double m_lineDelta;
    double m_columnDelta;

    double m_unhandledLineDelta, m_unhandledColumnDelta;

    bool m_gestureEnded;
    bool m_couldBeDoubleClick;
    bool m_couldBeTrippleClick;

    int m_lastTouchCount;

    ActionDirection m_direction;
};

QPointF getCentroid(const QList<QPointF>& points);

// Link TouchState::* to pImpl
bool TouchState::registerTouchStart(const TouchEvent& ev) { return pImpl->registerTouchStart(ev); }
void TouchState::registerTouchUpdate(const TouchEvent& ev) { return pImpl->registerTouchUpdate(ev); }
void TouchState::registerTouchEnd(const TouchEvent& ev) { return pImpl->registerTouchEnd(ev); }
void TouchState::update() { return pImpl->update(); }

Qt::Key TouchState::popAsKeyEquivalent(const TerminalState& state) 
{
    Q_UNUSED(state);

    return pImpl->popAsKeyEquivalent(); 
}

int TouchState::popAsVerticalScroll(const TerminalState& state) 
{
    return pImpl->popAsVerticalScroll(state); 
}

TouchState::FeedbackType TouchState::popAsTouchFeedback(const TerminalState& state)
{
    return pImpl->popAsTouchFeedback(state);
}

seconds TouchState::popAsActionDelay(const TerminalState& state)
{
    return pImpl->popAsActionDelay(state);
}

Qt::MouseButton TouchState::popAsMouseButton(const TerminalState& state) 
{
    return pImpl->popAsMouseButton(state); 
}

TouchState::ActionType TouchState::getNextAction(const TerminalState& state) const
{ 
    return pImpl->getNextAction(state); 
}

QPointF TouchState::getLastPosition() const
{
    return pImpl->m_lastCenter;
}

QPointF TouchState::getStartPosition() const
{
    return pImpl->m_startCenter;
}

QPointF TouchState::getAverageVelocityInChars() const
{
    return QPointF { pImpl->getAverageXVelocity(), pImpl->getAverageYVelocity() };
}

TouchState::TouchEvent::TouchEvent(const QTouchEvent* ev)
{
    for (const QTouchEvent::TouchPoint& point : ev->touchPoints()) {
        touchPoints.append(point.pos());
    }
}

TouchState::TouchEvent::TouchEvent(const QList<QPointF>& points)
{
    touchPoints.append(points);
}

TouchState::TouchEvent::TouchEvent(std::initializer_list<QPointF> points)
{
    touchPoints.append(points);
}

TouchState::TouchState(TerminalFont* font)
    : pImpl(new Impl(font)) {}


// Implementation

TouchState::Impl::Impl(TerminalFont* font) 
    : m_font(font)
    , m_lastKeyResult (Qt::Key_unknown)
    , m_lineDelta   (0.0)
    , m_columnDelta (0.0)
    , m_unhandledLineDelta   (0.0)
    , m_unhandledColumnDelta (0.0)
    , m_lastTouchCount (0)
    , m_direction { UNDETERMINED_DIRECTION }
{
    
}

void TouchState::Impl::reset(const TouchEvent& initialEvent)
{
    time_point currentTime = std::chrono::system_clock::now();
    m_gestureDuration = 0.0;

    m_lineDelta = 0.0;
    m_columnDelta = 0.0;

    m_unhandledLineDelta = 0.0;
    m_unhandledColumnDelta = 0.0;

    m_lastAction = ACTION_NULL;
    m_lastKeyResult = Qt::Key_unknown;
    m_direction = UNDETERMINED_DIRECTION;
    m_gestureEnded = false;

    m_lastTouchCount = initialEvent.touchPoints.length();

    m_lastCenter = getCentroid(initialEvent.touchPoints);
    m_startCenter = m_lastCenter;

    m_sequenceStartTime = currentTime;
}

bool TouchState::Impl::registerTouchStart(const TouchEvent& event)
{
    Q_ASSERT(m_font != nullptr);

    time_point currentTime = std::chrono::system_clock::now();
    duration_seconds timeSinceLastEventStart = currentTime - m_sequenceStartTime;

    // If the previous touch happened recently enough, this could be a double-click.
    bool couldBeDoubleClick = timeSinceLastEventStart.count() <= MAX_SECONDS_MOUSELIKE_EVENT;
    m_couldBeTrippleClick = m_couldBeDoubleClick && couldBeDoubleClick;
    m_couldBeDoubleClick = couldBeDoubleClick && !m_couldBeTrippleClick;

    reset(event);

    return true;
}

void TouchState::Impl::registerTouchUpdate(const TouchEvent& event)
{
    int touchCount = event.touchPoints.length();

    QPointF center = getCentroid(event.touchPoints);

    // If the number of touches increases, reset the sequence.
    if (touchCount > m_lastTouchCount) {
        reset(event);
    }

    double lineHeight = m_font->fontHeight() + m_font->lineSpacing();

    QPointF deltaPx = center - m_lastCenter;
    QPointF deltaChars { deltaPx.x() / m_font->fontWidth(), deltaPx.y() / lineHeight };

    m_lineDelta     -= deltaChars.y();
    m_columnDelta   += deltaChars.x();

    m_unhandledLineDelta    -= deltaChars.y();
    m_unhandledColumnDelta  += deltaChars.x();

    update();

    m_lastCenter = center;
}

void TouchState::Impl::update()
{
    if (m_gestureEnded) {
        return;
    }

    time_point currentTime = std::chrono::system_clock::now();
    duration_seconds timeSinceEventStart = currentTime - m_sequenceStartTime;
    m_gestureDuration = timeSinceEventStart.count();

    if (m_direction == UNDETERMINED_DIRECTION) {
        if (std::abs(m_lineDelta) >= GESTURE_DETERMINATION_LINES 
                    || std::abs(m_columnDelta) >= GESTURE_DETERMINATION_COLS) {
            if (std::abs(m_lineDelta) < std::abs(m_columnDelta)) {
                m_direction = HORIZONTAL_ACTION;
            }
            else {
                m_direction = VERTICAL_ACTION;
            }
        }
        else if (m_gestureDuration >= GESTURE_LONG_TAP_MIN_LENGTH) {
            m_direction = STATIONARY_ACTION;
        }
    }
}

void TouchState::Impl::registerTouchEnd(const TouchEvent& event)
{
    if (m_gestureEnded) {
        return;
    }

    registerTouchUpdate(event);

    if (m_direction == UNDETERMINED_DIRECTION 
            && m_gestureDuration >= GESTURE_TAP_MIN_LENGTH) {
        m_direction = STATIONARY_ACTION;
    }

    m_gestureEnded = true;
}

QPointF getCentroid(const QList<QPointF>& points)
{
    QPointF centroid {};

    for (const QPointF& point : points) {
        centroid += point;
    }

    centroid /= points.size();

    return centroid;
}

Qt::Key TouchState::Impl::peekKeyEquivalent() const
{
    Qt::Key result = Qt::Key_unknown;

    switch (m_direction) {
        case VERTICAL_ACTION:
            if (m_lastTouchCount == 1) {
                if (m_unhandledLineDelta >= 1.0) {
                    result = Qt::Key_Up;
                }
                else if (m_unhandledLineDelta <= -1.0) {
                    result = Qt::Key_Down;
                }
            }
            break;
        
        case HORIZONTAL_ACTION:
            if (m_unhandledColumnDelta >= 1.0) {
                if (m_lastTouchCount == 2) {
                    result = m_gestureEnded ? Qt::Key_Tab : Qt::Key_unknown;
                }
                else if (m_lastTouchCount == 4) {
                    result = m_gestureEnded ? Qt::Key_Enter : Qt::Key_unknown;
                }
                else {
                    result = Qt::Key_Right;
                }
            }
            else if (m_unhandledColumnDelta <= -1.0) {
                if (m_lastTouchCount == 2) {
                    result = m_gestureEnded ? Qt::Key_Escape : Qt::Key_unknown;
                }
                else {
                    result = Qt::Key_Left;
                }
            }

            break;
        
        default:
            break;
    }

    // If this sequence generates only one keypress (e.g. tab key), only
    // generate one!
    if ((result == Qt::Key_Tab || result == Qt::Key_Escape) && m_lastKeyResult == result) {
        result = Qt::Key_unknown;
    }

    return result;
}

Qt::Key TouchState::Impl::popAsKeyEquivalent()
{
    Qt::Key result = peekKeyEquivalent();

    if (result != Qt::Key_unknown) {
        if (m_direction == HORIZONTAL_ACTION) {
            m_unhandledLineDelta = 0.0;

            // We just turned a column of motion into
            // a keypress event! This is noted here:
            if (m_unhandledColumnDelta >= 1.0) {
                m_unhandledColumnDelta -= 1.0;
            }
            else if (m_unhandledColumnDelta <= -1.0) {
                m_unhandledColumnDelta += 1.0;
            }
        }
        else if (m_direction == VERTICAL_ACTION) {
            m_unhandledColumnDelta = 0.0;

            // We just turned a line of motion into a keypress!
            // We note this here:
            if (m_unhandledLineDelta >= 1.0) {
                m_unhandledLineDelta -= 1.0;
            }
            else if (m_unhandledLineDelta <= -1.0) {
                m_unhandledLineDelta += 1.0;
            }
        }
    }

    if (result != Qt::Key_unknown) {
        m_lastKeyResult = result;

        if (m_lastAction != ACTION_KEYPRESS) {
            // When we decide it's a keypress, there may
            //be unhandled lines/columns. Only interpret these
            //as a single keypress.
            m_unhandledLineDelta = 0.0;
            m_unhandledColumnDelta = 0.0;
        }
    }

    m_lastAction = ACTION_KEYPRESS;

    return result;
}

Konsole::TouchState::FeedbackType TouchState::Impl::peekTouchFeedback() const
{
    if (m_lastAction == ACTION_SHOW_FEEDBACK) {
        return NO_FEEDBACK;
    }

    if (m_direction == STATIONARY_ACTION 
            && m_gestureDuration > GESTURE_LONG_TAP_MIN_LENGTH) {
        return LONG_TAP_FEEDBACK;
    }

    return NO_FEEDBACK;
}

Konsole::TouchState::FeedbackType TouchState::Impl::popAsTouchFeedback(const TerminalState& state)
{
    FeedbackType result = peekTouchFeedback();

    // Should only fire once.
    ActionType currentAction = getNextAction(state);
    if (currentAction != ACTION_NULL) {
        m_lastAction = currentAction;
    }

    return result;
}

seconds TouchState::Impl::peekActionDelay() const
{
    //  Return an invalid delay if checking for a new
    // action after a delay is unimportant.
    if (m_direction != UNDETERMINED_DIRECTION) {
        return -1.0;
    }

    return PEEK_ACTION_EXTRA_DELAY
         + GESTURE_LONG_TAP_MIN_LENGTH - m_gestureDuration;
}

seconds TouchState::Impl::popAsActionDelay(const TerminalState& state)
{
    m_lastAction = getNextAction(state);

    return peekActionDelay();
}

Qt::MouseButton TouchState::Impl::peekMouseButton() const {
    Qt::MouseButton result = Qt::NoButton;

    bool couldBeLongRightClick = m_direction == STATIONARY_ACTION && m_gestureDuration >= STATIONARY_MIN_TIME_FOR_CONTEXT_MENU;

    if (couldBeLongRightClick || m_lastTouchCount == 2) {
        result = Qt::RightButton;
    }
    else if (m_lastTouchCount == 3) {
        result = Qt::MiddleButton;
    }
    else {
        result = Qt::LeftButton;
    }

    if (m_lastAction == ACTION_DRAG) {
        result = Qt::LeftButton;
    }

    return result;
}

Qt::MouseButton TouchState::Impl::popAsMouseButton(const TerminalState& state) {
    m_lastAction = getNextAction(state);
    Qt::MouseButton result = peekMouseButton();  // Can depend on m_lastAction

    m_unhandledColumnDelta = 0.0;
    m_unhandledLineDelta = 0.0;

    return result;
}

int TouchState::Impl::peekVerticalScroll() const
{
    if (m_direction != VERTICAL_ACTION) {
        return 0;
    }

    int scroll = std::floor(std::abs(m_unhandledLineDelta));

    if (m_unhandledLineDelta < 0.0) {
        scroll = -scroll;
    }
    
    return scroll;
}

int TouchState::Impl::popAsVerticalScroll(const TerminalState& state)
{
    int scroll = peekVerticalScroll();

    ActionType currentAction = getNextAction(state);
    if (currentAction != ACTION_NULL) {
        m_lastAction = currentAction;
    }
    
    m_unhandledLineDelta -= scroll;
    m_unhandledColumnDelta = 0.0;

    return scroll;
}

double TouchState::Impl::getAverageYVelocity() const
{
    return m_lineDelta / m_gestureDuration;
}

double TouchState::Impl::getAverageXVelocity() const
{
    return m_columnDelta / m_gestureDuration;
}

TouchState::ActionType TouchState::Impl::getNextAction(const TerminalState& state) const
{
    if (m_direction == UNDETERMINED_DIRECTION) {
        // Some actions are time-based, rather than touch-event based.
        //  For these, we need to check for the action after some
        // amount of time.
        if (m_lastAction != CHECK_FOR_ACTION_AFTER_DELAY
                && peekActionDelay() > 0.0) {
            return CHECK_FOR_ACTION_AFTER_DELAY;
        }

        return ACTION_NULL;
    }

    // If we didn't just fire an action that should happen only once per gesture,
    if (m_lastAction == ACTION_CLICK || m_lastAction == ACTION_DBCLICK || m_lastAction == ACTION_DRAG
            || m_lastAction == ACTION_TRIPPLE_CLICK || m_lastAction == ACTION_MOUSEBUTTON_UP
            || m_lastAction == ACTION_START_INERTIAL_SCROLL) {
        return ACTION_NULL;
    }

    // Continue any mousemove actions that have started.
    if (m_lastAction == ACTION_MOUSEBUTTON_MOVE || m_lastAction == ACTION_MOUSEBUTTON_DOWN) {
        if (m_gestureEnded) {
            return ACTION_MOUSEBUTTON_UP;
        }

        if (std::abs(m_unhandledLineDelta) > 1.0 
                || std::abs(m_unhandledColumnDelta) > 1.0) {
            return ACTION_MOUSEBUTTON_MOVE;
        }

        return ACTION_NULL;
    }

    bool haveLinesToScroll = peekVerticalScroll() != 0;
    bool gestureCouldBeKeypress = m_direction != STATIONARY_ACTION; // Stationary actions are clicks.
    bool haveValidKey = peekKeyEquivalent() != Qt::Key_unknown;
    bool couldScroll = !state.commandUsesMouseTracking && m_direction == VERTICAL_ACTION;
    bool tooFastForKeypress = false;

    // Single touch: Can be used for scrolling, selecting, or key press simulation.
    // Here:         Is it too fast to be interpreted as a key press?
    if (m_lastTouchCount == 1) {
        if (m_direction == HORIZONTAL_ACTION) {
            double speed = std::abs(getAverageXVelocity());
            tooFastForKeypress = speed > GESTURE_KEYPRESS_MAX_COL_SPEED;
        }
        else if (m_direction == VERTICAL_ACTION) {
            double velocity = getAverageYVelocity();
            double speed = std::abs(velocity);
            tooFastForKeypress = speed > GESTURE_KEYPRESS_MAX_LINE_SPEED;

            // If trying to scroll past the end, we don't have
            // a lower limit for gesture speed.
            if (state.scrolledToEnd && velocity > 0) {
                tooFastForKeypress = false;
            }
            else if (!state.haveScrollback && state.scrolledToEnd) {
                tooFastForKeypress = false;
            }
        }

        // A fast tap following the current gesture => interpret it as a mouse event, so
        // it's too fast to be a key press.
        tooFastForKeypress = tooFastForKeypress || m_couldBeDoubleClick;
    }

    // Two-finger vertical gestures should always scroll (mouse wheel or scroll).
    bool twoFingerVertical = m_lastTouchCount == 2 && m_direction == VERTICAL_ACTION;

    // Keep scrolling if scrolling before (and we have lines to scroll).
    // If done normal scrolling, start inertial scrolling!
    if (m_lastAction == ACTION_SCROLL || twoFingerVertical) {
        double speed = std::abs(getAverageYVelocity());

        // A fast enough flick should start inertial scrolling.
        if (m_gestureEnded && speed > GESTURE_FLING_MIN_LINE_SPEED) {
            return ACTION_START_INERTIAL_SCROLL;
        }
        // If m_unhandledLineDelta is big enough, scroll!
        else if (haveLinesToScroll) {
            return ACTION_SCROLL;
        }

        return ACTION_NULL;
    }

    if (m_direction == STATIONARY_ACTION) {
        // If it was stationary, but has since moved,
        if (std::abs(m_lineDelta) >= GESTURE_PRESS_TO_DRAG_DETERMINATION_LINES 
                || std::abs(m_columnDelta) >= GESTURE_PRESS_TO_DRAG_DETERMINATION_COLS) {
            return ACTION_DRAG;
        }
    }

    // If we can show feedback, do it! (E.g. a growing square for 
    // a right-click menu).
    if (peekTouchFeedback() != NO_FEEDBACK) {
        return ACTION_SHOW_FEEDBACK;
    }

    // If our last action was a keypress, continue that type of action.
    if (m_lastAction == ACTION_KEYPRESS && state.scrolledToEnd) {
        return haveValidKey ? ACTION_KEYPRESS : ACTION_NULL;
    }

    // If scrolled to the end and we can map the input to a key, do it!
    if (state.scrolledToEnd && !tooFastForKeypress && gestureCouldBeKeypress) {
        return haveValidKey ? ACTION_KEYPRESS : ACTION_NULL;
    }
    else if (couldScroll) {
        return haveLinesToScroll ? ACTION_SCROLL : ACTION_NULL;
    }

    //  We need to know how long the gesture took before we can take 
    // action (if a stationary gesture).
    //  If not ended, we should have handled it above.
    if (m_direction == STATIONARY_ACTION && !m_gestureEnded) {
        return ACTION_NULL;
    }

    // ...but if the gesture has ended, it's a click.
    if (m_direction == STATIONARY_ACTION) {
        if (m_couldBeDoubleClick) {
            return ACTION_DBCLICK;
        }

        if (m_couldBeTrippleClick) {
            return ACTION_TRIPPLE_CLICK;
        }

        return ACTION_CLICK;
    }

    // Don't act like a mouse if the gesture is a multi-finger swipe,
    //as this may lead to keyboard input after the gesture ends.
    if (m_lastTouchCount != 1 && !m_gestureEnded && state.scrolledToEnd) {
        return ACTION_NULL;
    }

    // Otherwise, act like a mouse.
    if (m_lastAction == ACTION_NULL || m_lastAction == CHECK_FOR_ACTION_AFTER_DELAY) {
        return ACTION_MOUSEBUTTON_DOWN;
    }

    // Other mouse handling is done above.

    return ACTION_NULL;
}

// Needed to allow a unique_ptr to an incomplete type in TouchState.h
// See https://stackoverflow.com/questions/9954518/stdunique-ptr-with-an-incomplete-type-wont-compile
TouchState::~TouchState() { }
