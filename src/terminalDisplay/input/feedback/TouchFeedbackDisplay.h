/*
    SPDX-FileCopyrightText: 2020-2020 Gustavo Carneiro <gcarneiroa@hotmail.com>
    SPDX-FileCopyrightText: 2007-2008 Robert Knight <robertknight@gmail.com>
    SPDX-FileCopyrightText: 1997, 1998 Lars Doelle <lars.doelle@on-line.de>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#pragma once

#include <memory>
#include <QPoint>

class QPainter;
class QWidget;

namespace Konsole {

/**
 * @brief Paints & manages touch feedback animations.
 */
class TouchFeedbackDisplay {
public:
    TouchFeedbackDisplay(QWidget* parent);
    ~TouchFeedbackDisplay();

    /**
     * @brief Start a long tap animation at the given point.
     * @param origin The center-point of the animation.
     */
    void startLongTapAnimation(QPoint origin);

    /**
     * @brief End the most-recently-started long tap animation.
     * 
     * Does nothing if there are no animations to stop.
     */
    void popLongTapAnimation();

    /**
     * @brief Draw all animations.
     */
    void render(QPainter& painter) const;

    /**
     * @returns whether there are any still-running animations.
     */
    bool hasFeedback() const;

    /**
     * @brief Stop and remove all animations.
     */
    void reset();

private:
    class Impl; std::unique_ptr<Impl> pImpl;
};

}
