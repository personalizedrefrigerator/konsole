/*
    SPDX-FileCopyrightText: 2020-2020 Gustavo Carneiro <gcarneiroa@hotmail.com>
    SPDX-FileCopyrightText: 2007-2008 Robert Knight <robertknight@gmail.com>
    SPDX-FileCopyrightText: 1997, 1998 Lars Doelle <lars.doelle@on-line.de>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

// Stdlib
#include <memory>

// Qt
#include <QPoint>
#include <QPainter>

namespace Konsole
{

/**
 * @brief Feedback for a long tap.
 * 
 * This might indicate, for example, that
 * stopping the gesture will open a context menu.
 */
class LongTapAnimation
{
public:
    /**
     * @brief Start the animation now, at the given
     *  position.
     * @param position The screen position at which the 
     *  gesture is to start.
     */
    LongTapAnimation(QPoint position);
    ~LongTapAnimation();

    LongTapAnimation(const LongTapAnimation& other);
    const LongTapAnimation& operator=(const LongTapAnimation& other);

    /**
     * Render the gesture based on the time since the gesture's start.
     */
    void render(QPainter& painter) const;

    /**
     * @returns the center point of this.
     */
    QPoint getPosition() const;

private:
    class Impl; std::unique_ptr<Impl> pImpl;
};

}
