/*
    SPDX-FileCopyrightText: 2020-2020 Gustavo Carneiro <gcarneiroa@hotmail.com>
    SPDX-FileCopyrightText: 2007-2008 Robert Knight <robertknight@gmail.com>
    SPDX-FileCopyrightText: 1997, 1998 Lars Doelle <lars.doelle@on-line.de>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "TouchFeedbackDisplay.h"

#include "LongTapAnimation.h"

// Stdlib
#include <chrono>
#include <cmath>
#include <deque>

// Qt
#include <QPoint>
#include <QPainter>
#include <QTimer>
#include <QWidget>

using namespace Konsole;

constexpr int ANIMATION_UPDATE_TIMEOUT = 10; // ms


class TouchFeedbackDisplay::Impl
{
public:
    Impl(QWidget* parent) : _parent(parent) { };

    void render(QPainter& painter) const;
    
    void popLongTapAnimation();
    void startLongTapAnimation(QPoint position);

    void reset();
    bool hasFeedback() const;
private:
    void animationUpdateLoop();

    QWidget* _parent;
    std::deque<LongTapAnimation> _rightClickAnimations;
    bool _runningAnimationUpdateLoop;
};

TouchFeedbackDisplay::TouchFeedbackDisplay(QWidget* parent) 
    : pImpl   {new Impl(parent)}
{

}

// Destructor allows freeing of forward-declared pImpl
TouchFeedbackDisplay::~TouchFeedbackDisplay() { }

// Connect methods from TouchFeedbackDisplay to TouchFeedbackDisplay::Impl
void TouchFeedbackDisplay::startLongTapAnimation(QPoint origin)
{
    pImpl->startLongTapAnimation(origin);
}

void TouchFeedbackDisplay::popLongTapAnimation()
{
    pImpl->popLongTapAnimation();
}

void TouchFeedbackDisplay::render(QPainter& painter) const
{
    pImpl->render(painter);
}

bool TouchFeedbackDisplay::hasFeedback() const
{
    return pImpl->hasFeedback();
}

void TouchFeedbackDisplay::reset() 
{
    pImpl->reset();
}


void TouchFeedbackDisplay::Impl::render(QPainter& painter) const
{
    for (const LongTapAnimation& anim : _rightClickAnimations) {
        anim.render(painter);
    }
}

void TouchFeedbackDisplay::Impl::animationUpdateLoop()
{
    _parent->repaint();

    if (!hasFeedback()) {
        _runningAnimationUpdateLoop = false;
        return;
    }

    QTimer::singleShot(ANIMATION_UPDATE_TIMEOUT, [=] () {
        this->animationUpdateLoop();
    });
}

void TouchFeedbackDisplay::Impl::startLongTapAnimation(QPoint position)
{
    _rightClickAnimations.push_back
    (
        LongTapAnimation { position }
    );

    if (!_runningAnimationUpdateLoop) {
        _runningAnimationUpdateLoop = true;
        animationUpdateLoop();
    }
}

void TouchFeedbackDisplay::Impl::popLongTapAnimation()
{
    if (!_rightClickAnimations.empty()) {
        _rightClickAnimations.pop_front();
    }
}

void TouchFeedbackDisplay::Impl::reset()
{
    bool hadFeedback = hasFeedback();
    _rightClickAnimations.clear();

    if (hadFeedback) {
        _parent->repaint();
    }
}

bool TouchFeedbackDisplay::Impl::hasFeedback() const
{
    return !_rightClickAnimations.empty();
}
