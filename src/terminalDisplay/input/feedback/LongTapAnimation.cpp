/*
    SPDX-FileCopyrightText: 2020-2020 Gustavo Carneiro <gcarneiroa@hotmail.com>
    SPDX-FileCopyrightText: 2007-2008 Robert Knight <robertknight@gmail.com>
    SPDX-FileCopyrightText: 1997, 1998 Lars Doelle <lars.doelle@on-line.de>

    SPDX-License-Identifier: GPL-2.0-or-later
*/
#include "LongTapAnimation.h"

// Stdlib
#include <chrono>
#include <cmath>

// Qt
#include <QPainter>
#include <QColor>
#include <QPen>
#include <QRect>
#include <QSize>
#include <QPainterPath>

using namespace Konsole;

using time_point = std::chrono::time_point<std::chrono::system_clock>;
using duration_seconds = std::chrono::duration<double, std::ratio<1, 1> >;

constexpr QColor TAP_FEEDBACK_COLOR { 147, 206, 233, 100 };
constexpr QColor TAP_FEEDBACK_OUTLINE_COLOR { 147, 206, 233, 255 };
constexpr QSize  TAP_FEEDBACK_MAX_SIZE { 70, 70 };
constexpr double TAP_FEEDBACK_TRANSITION_DURATION_SECONDS = 0.1;

constexpr double TAP_FEEDBACK_OUTLINE_WIDTH = 2;

class LongTapAnimation::Impl
{
public:
    Impl(QPoint position);

    void render(QPainter&) const;
    QPoint getPosition() const { return _position; };

private:
    QPoint _position;
    time_point _startTime;
};


LongTapAnimation::Impl::Impl(QPoint position)
    : _position(position)
    , _startTime { std::chrono::system_clock::now() }
{

}

LongTapAnimation::LongTapAnimation(QPoint position)
    : pImpl(new Impl(position))
{

}

LongTapAnimation::~LongTapAnimation() { }

LongTapAnimation::LongTapAnimation(const LongTapAnimation& other)
    : pImpl(new Impl(other.getPosition()))
{
    
}

void LongTapAnimation::Impl::render(QPainter& painter) const
{
    time_point now = std::chrono::system_clock::now();
    duration_seconds gestureDuration = now - _startTime;

    double fractionComplete = gestureDuration.count()
         / TAP_FEEDBACK_TRANSITION_DURATION_SECONDS;

    // We're at most 100% through the animation
    fractionComplete = std::min(1.0, fractionComplete);

    // Linearly change the size of the square we're drawing.
    QSize feedbackSize = TAP_FEEDBACK_MAX_SIZE * fractionComplete;
    QPoint topLeft { _position.x() - feedbackSize.width() / 2, 
        _position.y() - feedbackSize.height() / 2 };

    // Shape
    QRect rect { topLeft, feedbackSize };
    QPainterPath outline { };
    outline.addRect(rect);

    // Colors & styles
    QPen outlinePen { QBrush { TAP_FEEDBACK_OUTLINE_COLOR }, TAP_FEEDBACK_OUTLINE_WIDTH };

    // Draw it!
    painter.fillRect(rect, TAP_FEEDBACK_COLOR);
    painter.strokePath(outline, outlinePen);
}

void LongTapAnimation::render(QPainter& painter) const
{
    pImpl->render(painter);
}

QPoint LongTapAnimation::getPosition() const
{
    return pImpl->getPosition();
}
