/*
    SPDX-FileCopyrightText: 2015 Lindsay Roberts <os@lindsayr.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef SCROLLSTATE_H
#define SCROLLSTATE_H

#include <QPoint>

#include <memory>
class QWheelEvent;

namespace Konsole {
/**
 * Represents accumulation of wheel scroll from scroll events.
 *
 * Modern high precision scroll events supply many smaller events
 * that may or may not translate into a UI action to support smooth
 * pixel level scrolling. Builtin classes such as QScrollBar
 * support these events under Qt5, but custom code
 * written to handle scroll events in other ways must be modified
 * to accumulate small deltas and act when suitable thresholds have
 * been reached (ideally 1 for pixel scroll values towards any action
 * that can be mapped to a pixel movement).
 */
class ScrollState
{
public:
    enum {
        DEFAULT_ANGLE_SCROLL_LINE = 120
    };

    enum {
        DEGREES_PER_ANGLE_UNIT = 8
    };

    static inline int degreesToAngle(const int angle)
    {
        return angle * DEGREES_PER_ANGLE_UNIT;
    }

    int angle() const;
    int pixel() const;

    /** Add scroll values from a QWheelEvent to the accumulated totals */
    void addWheelEvent(const QWheelEvent *wheel);

    /**
     *  Resets inertial scrolling with the given initial
     * @p flickVelocity in lines per second. 
     * 
     * If @p flickVelocity is large enough, starts
     * inertial scrolling, else, stops inertial scrolling.
     * 
     * @p sourcePosition is the position of the event that caused
     * the reset.
     */
    void startInertialScrolling(double flickVelocity, QPoint sourcePosition);
    QPoint getInertialScrollSourcePosition() const;

    /**
     * If inertial scrolling, stop. Else, do nothing.
     * This is equivalent to calling startInertialScrolling
     * with a flick velocity of 0.0 lines/second.
     */
    void stopInertialScrolling();

    /**
     * Consumes lines of inertial scroll. 
     * 
     * @returns the number of accumulated lines of vertical scroll, signed
     *  to indicate direction.
     */
    int consumeInertialScrollLines();

    /**
     * @returns the current inertial scroll velocity in lines per second.
     * If positive, we're scrolling up. If negative, we're scrolling down.
     */
    double getInertialScrollVelocity() const;
    bool isInertialScrolling();

    /** Clear all - used when scroll is consumed by another class like QScrollBar */
    void clearAll();

    /** Return the (signed) multiple of @p stepsize available and subtract it from
     * accumulated totals. Also clears accumulated pixel scroll. */
    int consumeLegacySteps(int stepsize);

    /** Return the (signed) multiple of @p pixelStepSize if any pixel scroll is
     * available - that is, if pixel scroll is being supplied, or the same from
     * the @p angleStepSize else. The corresponding value is subtracted from
     * the accumulated total. The other scroll style value is cleared. */
    int consumeSteps(int pixelStepSize, int angleStepSize);

    ScrollState();
    ~ScrollState();

private:
    class Impl; std::unique_ptr<Impl> pImpl;
};
}

#endif // SCROLLSTATE_H
